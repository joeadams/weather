#!/bin/sh
config="./src/main/resources/application.conf"
placeholder=ENTER_OPEN_WEATHER_KEY
if grep --quiet $placeholder $config; then
 read -p "Please enter open weather key: " key
 sed -i "s/$placeholder/$key/" $config
else
  echo "placeholder for key has been replaced."
fi


