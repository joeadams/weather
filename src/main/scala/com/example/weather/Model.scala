package com.example.weather

import enumeratum.{CirceEnum, Enum, EnumEntry}
import io.circe.generic.JsonCodec
import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec}

object Model {

  sealed trait TemperatureRange extends EnumEntry
  case object TemperatureRange extends Enum[TemperatureRange] with CirceEnum[TemperatureRange] {
    val values = findValues
    case object COLD extends TemperatureRange
    case object MODERATE extends TemperatureRange
    case object HOT extends TemperatureRange

  }

  implicit val config: String => String = Configuration.snakeCaseTransformation

  case class Coordinates(latitude: BigDecimal, longitude: BigDecimal)

  case class WeatherError(error: Throwable) extends RuntimeException

  @JsonCodec case class OutboundResponse(
      temperatureRange: TemperatureRange,
      weather: List[String],
      alerts: List[Alert]
  )
  @JsonCodec case class OpenWeatherResponse(current: Current, alerts: Option[List[Alert]])

  @JsonCodec case class Current(temp: BigDecimal, weather: Option[List[Weather]])

  @JsonCodec case class Weather(description: String)

  implicit val customConfig: Configuration =
    Configuration.default.withSnakeCaseMemberNames
  @ConfiguredJsonCodec case class Alert(
      senderName: String,
      event: String,
      description: String,
      tags: List[String],
      start: Long,
      end: Long
  )

}
