package com.example.weather

import java.time.Instant

import cats.effect.Concurrent
import cats.implicits._
import com.example.weather.Model.{
  Coordinates,
  OpenWeatherResponse,
  OutboundResponse,
  TemperatureRange,
  WeatherError
}
import org.http4s.Uri
import org.http4s.circe.CirceEntityCodec.circeEntityDecoder
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl.io.GET
import org.http4s.implicits.http4sLiteralsSyntax

object OpenWeatherClient {

  trait Fetch[F[_]] {
    def get(coordinates: Coordinates): F[OutboundResponse]
  }

  private def coordinateParameters(coordinates: Coordinates): Map[String, String] =
    Map("lat" -> coordinates.latitude.toString(), "lon" -> coordinates.longitude.toString())

  private val constantParameters = Map(
    "appid" -> WeatherConfig.config.openWeatherKey,
    "exclude" -> "minutely,hourly,daily",
    "units" -> "imperial"
  )
  private val baseUri: Uri =
    uri"https://api.openweathermap.org/data/2.5/onecall".withQueryParams(constantParameters)

  class FetchImpl[F[_]: Concurrent](client: Client[F]) extends Fetch[F] {
    val dsl = new Http4sClientDsl[F] {}

    import dsl._

    override def get(coordinates: Coordinates): F[OutboundResponse] = {
      val uri =
        baseUri.withQueryParams(coordinateParameters(coordinates))
      client
        .expect[OpenWeatherResponse](GET(uri))
        .adaptError { case t =>
          WeatherError(t)
        }
        .map { openWeatherResponse =>
          val temperatureRange = openWeatherResponse.current.temp match {
            case t if t >= 85 => TemperatureRange.HOT
            case t if t <= 40 => TemperatureRange.COLD
            case _ => TemperatureRange.MODERATE
          }
          val weather =
            openWeatherResponse.current.weather.toList
              .flatMap(_.map(_.description))

          val now = Instant.now().getEpochSecond()
          val alerts = openWeatherResponse.alerts.toList.flatMap(
            _.filter(alert => alert.start <= now && now <= alert.end)
          )
          OutboundResponse(temperatureRange = temperatureRange, weather = weather, alerts = alerts)

        }
    }
  }

}
