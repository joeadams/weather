package com.example.weather

import cats.effect.Sync
import cats.implicits._
import com.example.weather.Model.Coordinates
import com.example.weather.OpenWeatherClient.Fetch
import org.http4s.HttpRoutes
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.io.QueryParamDecoderMatcher

object WeatherRoutes {
  object LatitudeParam extends QueryParamDecoderMatcher[Double]("lat")
  object LongitudeParam extends QueryParamDecoderMatcher[Double]("lon")

  def weatherRoutes[F[_]: Sync](getOpenWeatherReport: Fetch[F]): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    HttpRoutes.of[F] {
      case GET ->
          Root / "weather" :?
          LatitudeParam(latitude) +&
          LongitudeParam(longitude) =>
        getOpenWeatherReport
          .get(Coordinates(latitude = latitude, longitude = longitude))
          .flatMap(Ok(_))

    }
  }
}
