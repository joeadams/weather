package com.example.weather

import java.nio.file.ProviderNotFoundException

import io.circe.Decoder
import io.circe.config.parser
import io.circe.generic.auto._
import io.circe.generic.semiauto.deriveDecoder

final case class WeatherConfig(host: String, port: Int, openWeatherKey: String)

object WeatherConfig {
  implicit val configDecoder: Decoder[WeatherConfig] = deriveDecoder
  case class Container(weatherConfig: WeatherConfig)
  val config: WeatherConfig = parser.decode[Container]() match {
    case Right(container: Container) => container.weatherConfig
    case Left(error) => throw new ProviderNotFoundException(error.getMessage)
  }
}
