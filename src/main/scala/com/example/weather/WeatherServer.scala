package com.example.weather

import cats.effect.{Async, Resource}
import cats.syntax.all._
import com.comcast.ip4s._
import com.example.weather.OpenWeatherClient.FetchImpl
import fs2.Stream
import org.http4s.ember.client.EmberClientBuilder
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits._
import org.http4s.server.middleware.Logger

object WeatherServer {

  def stream[F[_]: Async]: Stream[F, Nothing] = {

    for {
      client <- Stream.resource(EmberClientBuilder.default[F].build)
      httpApp = WeatherRoutes.weatherRoutes[F](new FetchImpl(client)).orNotFound

      finalHttpApp = Logger.httpApp(true, true)(httpApp)
      exitCode <- Stream.resource(
        EmberServerBuilder
          .default[F]
          .withHostOption(IpAddress.fromString(WeatherConfig.config.host))
          .withPort(Port.fromInt(WeatherConfig.config.port).get)
          .withHttpApp(finalHttpApp)
          .build >>
          Resource.eval(Async[F].never)
      )
    } yield exitCode
  }.drain
}
