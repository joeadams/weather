package com.example.weather

import com.example.weather.Model.{
  Alert,
  Current,
  OpenWeatherResponse,
  OutboundResponse,
  TemperatureRange,
  Weather
}
import munit.CatsEffectSuite

import scala.io.Source
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

class ParseSpec extends CatsEffectSuite {

  def readResource(filename: String) =
    Source.fromResource(filename).getLines().mkString("\n")

  test("decodes alert") {
    decode[Alert](readResource("alert.json")) match {
      case Right(Alert(_, _, _, _, _, _)) => assert(true)
      case Left(error) => fail(error.getMessage)
    }
  }

  test("decodes alerts") {
    decode[List[Alert]](readResource("alerts.json")) match {
      case Right(alerts) =>
        assert(4 == alerts.length, s"alert length should be four it was ${alerts.length}")
      case Left(error) => fail(error.getMessage)
    }
  }

  test("decodes weather") {
    decode[Weather](readResource("weather.json")) match {
      case Right(Weather(_)) => assert(true)
      case Left(error) => fail(error.getMessage)
    }
  }

  test("decodes current") {
    decode[Current](readResource("current.json")) match {
      case Right(Current(_, _)) => assert(true)
      case Left(error) => fail(error.getMessage)
    }
  }

  test("decodes response with alerts") {
    decode[OpenWeatherResponse](readResource("with_alerts.json")) match {
      case Right(report) => println(report)
      case Left(error) => fail(error.getMessage)
    }
  }

  test("decodes response without alerts") {
    decode[OpenWeatherResponse](readResource("no_alerts.json")) match {
      case Right(report) => println(report)
      case Left(error) => fail(error.getMessage)
    }
  }

  test("encode outbound Response") {
    val outbound = OutboundResponse(TemperatureRange.MODERATE, List(), List())
    val json: Json = outbound.asJson
    println(json.toString())
  }

}
