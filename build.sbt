name := "weather"
version := "0.0.1-SNAPSHOT"
scalaVersion := "2.13.6"
resolvers += Resolver.sonatypeRepo("snapshots")

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-unchecked",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Yrangepos",
  "-Ymacro-annotations",
  "-Ywarn-unused:imports"
)

val Http4sVersion = "0.23.6"
val CirceVersion = "0.14.1"
val CirceGenericExVersion = "0.14.1"
val CirceConfigVersion = "0.8.0"
val EnumeratumCirceVersion = "1.7.0"
val MunitVersion = "0.7.29"
val LogbackVersion = "1.2.6"
val MunitCatsEffectVersion = "1.0.6"
libraryDependencies ++= Seq(
  "org.http4s"         %%  "http4s-ember-server"  %  Http4sVersion,
  "org.http4s"         %%  "http4s-ember-client"   %  Http4sVersion,
  "org.http4s"         %%  "http4s-circe"               %  Http4sVersion,
  "org.http4s"         %%  "http4s-dsl"                  %  Http4sVersion,
  "io.circe"              %%  "circe-core"                  %  CirceVersion,
  "io.circe"              %%  "circe-generic"             %  CirceVersion,
  "io.circe"              %%  "circe-parser"               %  CirceVersion,
  "io.circe"              %%  "circe-literal"                %  CirceVersion,
  "io.circe"              %%  "circe-generic-extras"  %  CirceGenericExVersion,
  "io.circe"              %%  "circe-config"               %  CirceConfigVersion,
  "com.beachape"  %%  "enumeratum-circe"    % EnumeratumCirceVersion,
  "org.scalameta"   %%  "munit"                        %  MunitVersion                    % Test,
  "org.typelevel"     %%  "munit-cats-effect-3"   %  MunitCatsEffectVersion   % Test,
  "ch.qos.logback"  %     "logback-classic"         %  LogbackVersion,
)

addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.13.0" cross CrossVersion.full)
addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1")
testFrameworks += new TestFramework("munit.Framework")
enablePlugins(ScalafmtPlugin)


